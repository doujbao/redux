import React from 'react'

import { HashRouter as Router, Route, Redirect, Switch, Link } from 'react-router-dom'

import Mob from './stateCollection/mob'
import ReactRedux from './stateCollection/reactRedux'
import Redux from './stateCollection/redux'

function App() {
  return (
    <div className="App">

      <Router>

        <div style={{ marginBottom: '20px' }}><Link to='/mob'>mob</Link>|<Link to='/reactRedux'>react-redux</Link>|<Link to='/redux'>redux</Link></div>

        <Switch>
          <Redirect exact path='/' to='/reactRedux'></Redirect>

          <Route path='/mob' component={Mob} />

          <Route path='/reactRedux' component={ReactRedux} />

          <Route path='/redux' component={Redux} />

        </Switch>

      </Router>

    </div>
  );
}

export default App;
