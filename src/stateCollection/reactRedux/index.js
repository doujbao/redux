// react-redux

import React, { Component } from 'react'

import { connect } from 'react-redux'

import action from './action'

class ReactComponent extends Component {

    render() {

        const { count = 0, setCount } = this.props

        return (<div>

            react-redux

            <div>count:{count}</div>

            <button onClick={setCount}>add-count</button>

        </div>)

    }
}

const mapStateToProps = (state) => {
    return {
        count: state.count
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setCount: () => dispatch(action.setCount)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReactComponent)