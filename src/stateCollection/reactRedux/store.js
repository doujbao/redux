/**
 * @description 数据容器
 */

import { createStore } from 'redux'

import reducer from './reducer'

//导出容器
export default createStore(reducer)