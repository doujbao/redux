/**
 * @description 这个对象可以帮我操作某个对象
 */

//默认数据
const initState = {
    count: 0
}

export default function reducer(state = initState, action) {
    switch (action.type) {
        case 'SET_COUNT':
            return { count: state.count + 1 }
        default:
            return state
    }
}