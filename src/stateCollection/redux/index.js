/**
 * redux
 * 单一数据源|State 是只读的|使用纯函数来执行修改
 */

import React, { Component } from 'react'

class ReactComponent extends Component {
    render() {
        return (<div>
            redux
        </div>)
    }
}

export default ReactComponent